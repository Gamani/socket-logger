package ru.zoom4ikdan4ik.logger.sockets;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class ClientSocket {
    private final InetAddress inetAddress;
    private final int port;
    private Socket socket;

    public ClientSocket(InetAddress inetAddress, int port) throws IOException {
        this.inetAddress = inetAddress;
        this.port = port;

        this.setConnection();
    }

    public final void writeSocket(List<String> lines) throws IOException {
        if (this.socket.isClosed()) this.setConnection();
        if (this.socket.isConnected())
            try (OutputStream outputStream = this.socket.getOutputStream()) {
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

                for (String string : lines)
                    dataOutputStream.writeUTF(string);

                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    private void setConnection() throws IOException {
        this.socket = new Socket(this.inetAddress, this.port);

        this.socket.setKeepAlive(true);
    }
}
