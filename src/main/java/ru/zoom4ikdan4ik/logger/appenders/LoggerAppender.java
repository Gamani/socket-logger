package ru.zoom4ikdan4ik.logger.appenders;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import ru.zoom4ikdan4ik.logger.ModLogger;
import ru.zoom4ikdan4ik.logger.sockets.ClientSocket;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoggerAppender extends AbstractAppender {
    private ClientSocket clientSocket = ModLogger.instance.getClientSocket();
    private List<String> list = new ArrayList();

    public LoggerAppender(String name, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
    }

    public LoggerAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignore) {
        super(name, filter, layout, ignore);
    }

    @Override
    public void append(LogEvent event) {
        byte[] data = this.getLayout().toByteArray(event);

        this.list.add(new String((data)));
    }

    @Override
    public void stop() {
        System.out.println("LOGGER MY FUCK");
        try {
            this.clientSocket.writeSocket(this.list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
